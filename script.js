/* Теоретичні питання.
1. У JavaScript цикли це управляючі структури, які дозволяють повторно виконувати якийсь блок коду, поки задана умова є істиною.
2. У JavaScript існує 3 види циклів: 'for', 'while', 'do while'.
3. Оператор 'while' оцінює вираз перед кожною ітерацією циклу. Якщо вираз оцінюється як `true`, оператор `while` виконує
   вказаний блок коду. В іншому випадку цикл `while` завершує свою роботу. На відміну від циклу `while`, оператор
   `do...while` завжди виконує цикл  принаймні один раз, перед оцінкою виразу.
 */
'use strict'

let number1 =  prompt('Введіть перше число');
let number2 = prompt('Введіть друге число');
while(isNaN(number1)){
     number1= prompt('Введіть будь ласка число');
    break;
}
while(isNaN(number2)){
    number2 = prompt('Введіть будь ласка число');
    break;
}
let maxNumber = Math.max(number1, number2);
for(let minNumber = Math.min(number1, number2); ; ) {
   alert(`Введені числа: ${minNumber}, ${maxNumber}`);
   break;
 }


let data = +prompt('Введіть будь ласка число ');
while(data % 2 !== 0) {
      data =  +prompt('Введіть будь ласка число ');
}
